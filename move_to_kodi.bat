@echo off

rmdir /s /y _kodi\portable_data\addons\script.module.chori.accounts
xcopy /q /e /y script.module.chori.accounts  _kodi\portable_data\addons\script.module.chori.accounts\

rmdir /s /y _kodi\portable_data\addons\plugin.video.venom
xcopy /q /e /y plugin.video.venom  _kodi\portable_data\addons\plugin.video.venom\

rmdir /s /y _kodi\portable_data\addons\plugin.video.fen
xcopy /q /e /y plugin.video.fen  _kodi\portable_data\addons\plugin.video.fen\

rmdir /s /y _kodi\portable_data\addons\plugin.video.chori
xcopy /q /e /y plugin.video.chori  _kodi\portable_data\addons\plugin.video.chori\

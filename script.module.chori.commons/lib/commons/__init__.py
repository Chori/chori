import os, xbmc, json


def enable_proxy():
    response = xbmc.executeJSONRPC(
        '{"jsonrpc":"2.0","method":"Settings.GetSettingValue", "params":{"setting":"network.usehttpproxy"},"id":1}'
    )
    if json.loads(response)["result"]["value"]:
        needed_data = [
            "network.httpproxytype",
            "network.httpproxyserver",
            "network.httpproxyport",
            "network.httpproxyusername",
            "network.httpproxypassword",
        ]
        proxy_settings = dict()

        for idx, name in enumerate(needed_data):
            response = xbmc.executeJSONRPC(
                '{"jsonrpc":"2.0","method":"Settings.GetSettingValue", "params":{"setting":"%s"},"id":%s}' % (name, idx + 2)
            )
            xbmc.log(response)
            proxy_settings[name] = json.loads(response)["result"]["value"]
        if len(proxy_settings["network.httpproxyusername"]) and len(proxy_settings["network.httpproxypassword"]):
            user_part = "%s:%s@" % (proxy_settings["network.httpproxyusername"], proxy_settings["network.httpproxypassword"])
        elif len(proxy_settings["network.httpproxyusername"]):
            user_part = "%s@" % (proxy_settings["network.httpproxyusername"])
        else:
            user_part = ""

        proxy_type = ["http", "socks4a", "socks5", "socks5h"][proxy_settings["network.httpproxytype"] - 1]

        os.environ["HTTPS_PROXY"] = "%s://%s%s:%s" % (
            proxy_type,
            user_part,
            proxy_settings["network.httpproxyserver"],
            proxy_settings["network.httpproxyport"],
        )
        os.environ["HTTP_PROXY"] = "%s://%s%s:%s" % (
            proxy_type,
            user_part,
            proxy_settings["network.httpproxyserver"],
            proxy_settings["network.httpproxyport"],
        )
    else:
        os.environ.pop("HTTPS_PROXY")
        os.environ.pop("HTTP_PROXY")

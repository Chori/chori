# -*- coding: utf-8 -*-
from sys import argv
from modules.router import routing
from commons import enable_proxy

enable_proxy()
routing(argv[2])

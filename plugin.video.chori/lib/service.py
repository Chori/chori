# -*- coding: utf-8 -*-
from threading import Thread
from modules import service_functions
from modules.settings_reader import make_settings_dict, get_setting
from modules.kodi_utils import set_property, clear_property, sleep, xbmc_monitor, logger
from commons import enable_proxy


enable_proxy()


class ChoriMonitor(xbmc_monitor):
    def __init__(self):
        xbmc_monitor.__init__(self)
        logger("CHORI", "Main Monitor Service Starting")
        logger("CHORI", "Settings Monitor Service Starting")
        self.startUpServices()

    def startUpServices(self):
        threads = []
        functions = (service_functions.DatabaseMaintenance().run, service_functions.TraktMonitor().run)
        for item in functions:
            threads.append(Thread(target=item))
        while not self.abortRequested():
            try:
                service_functions.InitializeDatabases().run()
            except Exception:
                pass
            try:
                service_functions.CheckSettingsFile().run()
            except Exception:
                pass
            try:
                service_functions.SyncChoriAccounts().run()
            except Exception:
                pass
            [i.start() for i in threads]
            try:
                service_functions.ClearSubs().run()
            except Exception:
                pass
            try:
                service_functions.ViewsSetWindowProperties().run()
            except Exception:
                pass
            try:
                service_functions.AutoRun().run()
            except Exception:
                pass
            try:
                service_functions.ReuseLanguageInvokerCheck().run()
            except Exception:
                pass
            break

    def onScreensaverActivated(self):
        set_property("chori_pause_services", "true")

    def onScreensaverDeactivated(self):
        clear_property("chori_pause_services")

    def onSettingsChanged(self):
        clear_property("chori_settings")
        sleep(50)
        make_settings_dict()
        set_property("chori_kodi_menu_cache", get_setting("kodi_menu_cache"))

    def onNotification(self, sender, method, data):
        if method == "System.OnSleep":
            set_property("chori_pause_services", "true")
        elif method == "System.OnWake":
            clear_property("chori_pause_services")


ChoriMonitor().waitForAbort()

logger("CHORI", "Settings Monitor Service Finished")
logger("CHORI", "Main Monitor Service Finished")

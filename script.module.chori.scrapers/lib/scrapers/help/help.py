# -*- coding: utf-8 -*-
"""
	Chori Scrapers Module
"""

from scrapers.modules.control import addonPath, addonVersion, joinPath
from scrapers.windows.textviewer import TextViewerXML


def get(file):
	scrapers_path = addonPath()
	scrapers_version = addonVersion()
	helpFile = joinPath(scrapers_path, 'lib', 'scrapers', 'help', file + '.txt')
	r = open(helpFile, 'r', encoding='utf-8', errors='ignore')
	text = r.read()
	r.close()
	heading = '[B]Chori Scrapers -  v%s - %s[/B]' % (scrapers_version, file)
	windows = TextViewerXML('textviewer.xml', scrapers_path, heading=heading, text=text)
	windows.run()
	del windows
# -*- coding: utf-8 -*-
"""
	Chori Scrapers Module
"""

from scrapers.modules.control import addonPath, addonVersion, joinPath
from scrapers.windows.textviewer import TextViewerXML


def get():
	scrapers_path = addonPath()
	scrapers_version = addonVersion()
	changelogfile = joinPath(scrapers_path, 'changelog.txt')
	r = open(changelogfile, 'r', encoding='utf-8', errors='ignore')
	text = r.read()
	r.close()
	heading = '[B]Chori Scrapers -  v%s - ChangeLog[/B]' % scrapers_version
	windows = TextViewerXML('textviewer.xml', scrapers_path, heading=heading, text=text)
	windows.run()
	del windows
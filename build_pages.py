
import datetime as dt
import markdown
from jinja2 import Environment, FileSystemLoader, select_autoescape

with open("README.md", "r", encoding="utf-8") as input_file:
    text = input_file.read()

readme = markdown.markdown(
    text, output_format="html", extensions=["markdown.extensions.extra", "markdown.extensions.sane_lists"]
)

human_now = dt.datetime.utcnow().strftime("%dth %b %Y, %I:%M%p")

env = Environment(loader=FileSystemLoader("_pages"), autoescape=select_autoescape())
template = env.get_template("index.html.jinja")

with open("_pages/index.html", "w+", encoding="utf-8") as output_file:
    output_file.write(template.render(readme=readme, now=human_now))

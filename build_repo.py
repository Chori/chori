import create_repository as cr
import sys
import os

data_path = sys.argv[1]
info_basename = "addons.xml"
info_path = os.path.join(data_path, info_basename)
checksum_path = "{}.md5".format(info_path)
addons = [
    "./repository.chori/",
    "./plugin.video.fen/",
    "./plugin.video.venom/",
    "./plugin.video.chori/",
    "./context.venom/",
    "./script.module.chori.scrapers/",
    "./script.module.metahandler/",
    "./script.module.chori.accounts/",
    "./script.module.chori.art/",
    "./script.module.chori.commons/",
]

cr.create_repository(addons, data_path, info_path, checksum_path, False, False)

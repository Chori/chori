# -*- coding: utf-8 -*-
"""
	Chori Accounts
"""

from accounts.modules.control import addonPath, addonVersion, joinPath
from accounts.windows.textviewer import TextViewerXML


def get():
	accounts_path = addonPath()
	accounts_version = addonVersion()
	changelogfile = joinPath(accounts_path, 'changelog.txt')
	r = open(changelogfile, 'r', encoding='utf-8', errors='ignore')
	text = r.read()
	r.close()
	heading = '[B]Chori Accounts -  v%s - ChangeLog[/B]' % accounts_version
	windows = TextViewerXML('textviewer.xml', accounts_path, heading=heading, text=text)
	windows.run()
	del windows
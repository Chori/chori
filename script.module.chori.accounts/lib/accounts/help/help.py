# -*- coding: utf-8 -*-
"""
	Chori Accounts
"""

from accounts.modules.control import addonPath, addonVersion, joinPath
from accounts.windows.textviewer import TextViewerXML

def get(file):
	accounts_path = addonPath()
	accounts_version = addonVersion()
	helpFile = joinPath(accounts_path, 'lib', 'accounts', 'help', file + '.txt')
	r = open(helpFile, 'r', encoding='utf-8', errors='ignore')
	text = r.read()
	r.close()
	heading = '[B]Chori Accounts -  v%s - %s[/B]' % (accounts_version, file)
	windows = TextViewerXML('textviewer.xml', accounts_path, heading=heading, text=text)
	windows.run()
	del windows
# Chori Accounts

# Welcome to the *Chori Accounts* Project
Kodi developers one stop shop for common account information accessed under one roof that all addons can share.  This allows users to setup accounts once and addons access that data.

Source and repository available at https://codeberg.org/Chori/chori

To add *Chori Accounts* as a deppendecy
add the following to your addon's addon.xml file between the `<requires></requires>` tag.

`<import addon="script.module.chori.accounts" version="2.1.0" />`
Addon developers can access *Chori Accounts* as simply as

# How to call *Chori Accounts* settings 

You can use `import accounts` once the dependecy is added to your addon.  The following lists the functions that can be called that will respond with a dictionary and the supplied key and value.

### accounts.getAll()
This function call will provide all available accounts as a dictionary for each account handled.  If we called `accounts = accounts.getAll()` The returned data will be as follows.
`accounts ={'premiumize': {'enabled': '', username': '', 'token': ''}, 'alldebrid': {'enabled': '', 'username': '', 'token': ''}, 'tmdb': {'username': '', 'password': '', 'api_key': '', 'session_id': ''},
	'realdebrid': {'enabled': '', 'username': '', 'token': '', 'secret': '', 'refresh': '', 'client_id': ''}, 'ororo': {'password': '', 'email': ''}, 'tvdb': {'api_key': ''}, 'filepursuit': {'api_key': ''},
	'trakt': {'api_secret': '', 'api_key': '', 'username': '', 'token': '', 'expires': '', 'refresh': ''}, 'imdb': {'user': ''}, 'easyNews': {'username': '', 'password': ''}, 'furk': {'username': '', 'api_key': '', 'password': ''},
	'fanart_tv': {'api_key': ''}}`

### accounts.getTrakt()
Returns trakt only account info
```trakt': {{'api_secret': '', 'api_key': '', 'username': '', 'token': '', 'expires': '', 'refresh': ''}```

### accounts.getAllDebrid()
Returns all debrid account information supported (currently All-Debrid, Premiumize.me, Real-Debrid)
There is an addional key for each dictionary `'enabled'`.  This key could be used by the adon developer that the debrid service is temporarily disabled by the user.
`{premiumize': {'enabled': '', 'username': '', 'token': ''}, 'alldebrid': {'enabled': '', 'username': '', 'token': ''}, 'realdebrid': {'enabled': '', 'username': '', 'token': '', 'secret': '', 'refresh': '', 'client_id': ''}}`

### accounts.getAD()
`alldebrid': {'enabled': '', 'username': '', 'token': ''}`
 
### accounts.getPM()
`premiumize': {'enabled': '', 'username': '', 'token': ''}`
  
### accounts.getRD()
`realdebrid': {'enabled': '', 'username': '', 'token': '', 'secret': '', 'refresh': '', 'client_id': ''}`

### accounts.getAllMeta()
`{'tmdb': {'username': '', 'password': '', 'api_key': '', 'session_id': ''}, 'tvdb': {'api_key': ''}, 'imdb': {'user': ''}, 'fanart_tv': {'api_key': ''}}`

### accounts.getFanart_tv()
`fanart_tv': {'api_key': ''}`

### accounts.getTMDb()
`tmdb': {'username': '', 'password': '', 'api_key': '', 'session_id': ''}`

### accounts.getTVDb()
`tvdb': {'api_key': ''}`

### accounts.getIMDb()
`imdb': {'user': ''}`

### getAllScraper()
`{'ororo': {'password': '', 'email': ''}, 'filepursuit': {'api_key': ''}, 'easyNews': {'username': '', 'password': ''}, 'furk': {'username': '', 'api_key': '', 'password': ''}}`

### getFilepursuit()
`filepursuit: {'api_key': ''}`

### accounts.getFurk()
`furk: {'username': '', 'api_key': '', 'password': ''}`

### accounts.getEasyNews()
`easyNews: {'username': '', 'password': ''}`

### accounts.getOrro()
`ororo: {'password': '', 'email': ''}`

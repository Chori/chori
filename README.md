# Chori Kodi Add-ons

The add-on once known as *Fen* found a new life here, not in an attempt to rewrite
history, but in an attempt to squeeze a little more life out of this great kodi
add-on where while honoring the original creator's wishes to not be associated
with any new developments.

Because the adjecent add-on  *Venom* was also abandoned, it is hosted here as well,
under it's original name. The reasoning being that, apart from doing the bare
minimum to keep it working *currently* (integrate properly with accounts and
scrapers) no further changes will happen.

*THE RENAMING IS A WORK IN PROGRESS, IT IS POSSIBLE THE OLD NAMES STILL POP UP IN
UNEXPECTED PLACES, FUTURE UPDATES WILL FIX THAT*

## Issues / pull requests

- Code is hosted at <https://codeberg.org/Chori/chori>
- Issues can be filed under
  [Issues](https://codeberg.org/Chori/chori/issues/new)
- Pull requests under [Pull Requests](https://codeberg.org/Chori/chori/pulls)
- Anything that doesn't fall under these, contact me by email: <rrknqaa6@anonaddy.me>

## Installing the Add-on(s)
For a detailed walkthrough, use google. For example
<https://www.firesticktricks.com/install-kodi-addon.html> explainst things well.
Terminology from afore mentioned guide is used.

(Please notice that the add-ons are build from the release branch, master is the
development branch, holding unpublished changes)

1. When moving from old venom/fen, your settings are **not** preserved, to avoid
confusion uninstall *My Accounts*, *fenomscrapers* and *tikiart* if you have
those installed **and are not needing them for other add-ons**. Everything else
will be overwritten.
2. Add `https://chori.cf/` as a kodi source (this is the **source
   URL**).
3. Install the repositori by choosing *Install from zip file* under add-ons, the
   file is `repository.chori.zip`.
4. Install the add-on from the repository named *Chori Repository*. For a clean
   sheet just install *Chori*. If you have shortcuts to *Fen*, you can install
   *Fen (shim)* to keep them working in a buggy hacky way.
   

## A little background; Naming, ID's and conflicts
The first round of updating fen was just hardcoding api-keys, and all was
peachy. However there seems to be some conflict about whether or not it is
proper to keep the original name of an add-on when fixing it up, in the end this
lead to the decision to rename *Fen* to *Chori*, unfortunately in Kodi names
purelu cosmetics, everything is identified by an ID, so these have to change as well. However *Fen* was distributed as a
combination of add-ons, what to do with these, especially the *My Accounts*
addons seems to have some legacy, originating from
<https://github.com/a4k-openproject/script.module.myaccounts>. Which brings the
question how to ensure comptability, in the end the decision was made that this
is too hard to ensure (the problem is api-keys, and authorization granted to
these api-keys, which makes it nonsensical to hardcode api-keys in a video add-on
while doing the authorization in the account manager add-on, both need the
**same** keys for authorization to be valid, thuse the choice was made to only add
api-keys to the accounts add-on), so *My Accounts* has been renamed to
*Chori Accounts*, less convenient for users (if you were using other addons
relying on *My Accounts* you'll need to keep both *My Accounts* and *Chori
Accounts*) but it avoids potential issues. The same goes for *fenomscrapers*,
even if that one was probably not widely used outside of *Fen* / *Venom*.

# Changes



## `plugin.video.chori` - Chori
### 2.7.1
- further adjustments of code to reflect new ID
- honor global proxy settings
### 2.7.0
- reorganize code to reflect new package ID's.
- changed id from `plugin.video.fen` to plugin.video.chori and rename to *Chori*
### 2.6.15
- add new hardcoded trakt keys

## `script.module.chori.scrapers` - Chori Scrapers
### 2.2.0
- honor global proxy settings
- changed id from `script.module.fenomscrapers` to `script.module.chori.scrapers` and rename to *Chori Scrapers*
### 2.1.10
- restored code from 2.1.8

## `script.module.chori.accounts` - Chori Accounts
### 2.1.0
- honor global proxy settings
- change update-check url to this repository
- add tmdb api keys
- add option to add own trakt api keys, improve settings for custom trakt and
  tmdb keys
- changed id from `script.module.myaccounts` to script.module.chori.accounts and rename to *Chori Accounts*
### 2.0.3
- add new hardcoded trakt keys

## `script.module.chori.accounts` - Chori Accounts
### 0.1.0
- add function to use kodi proxy settings
## `script.module.chori.art` - Chori Art
### 1.2.0
- changed id from `script.tikiart` to `script.module.chori.art` and rename to *Chori Art*

## `chori.repository` - Chori Repository
### 0.0.2
- point to repo on pages

## `plugin.video.fen` - Fen
### 2.6.16
- turn into a shim for Chori
### 2.6.15
- add new hardcoded trakt keys

## `plugin.video.venom` - Venom
### 6.5.9
**Unless someone decides to maintain/fork/do-something-with venom, this will be
the last change to it**

- remove hardcoded keys, properly use myaccounts keys
- use renamed accounts and scrapers
### 6.5.8
- add new hardcoded trakt keys

## `context.venom`
No new version

## `script.module.metahandler`
No new version